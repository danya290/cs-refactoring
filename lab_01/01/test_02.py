import unittest
from unittest.mock import patch
from io import StringIO
from task_02 import main

class TestAngleCalculation(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="s")
    def test_main_invalid_input(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        output = mock_stdout.getvalue().strip()

        self.assertEqual(output,"Некоректні дані. Введіть числове значення для кута a.")
        self.assertEqual(cm.exception.code, 1)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="45")
    def test_main(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)

        output = mock_stdout.getvalue().strip()
        self.assertIn("Z1", output)
        self.assertIn("Z2", output)
        self.assertIn("Z3", output)


if __name__ == '__main__':
    unittest.main()

