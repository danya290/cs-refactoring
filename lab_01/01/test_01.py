import unittest
from unittest.mock import patch
from io import StringIO
from task_01 import  main

class TestSquareAreaCalculator(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["-2"])
    def test_main_negative_diagonal(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = "Довжина діагоналі ({}) не може бути менше або дорівнювати 0.".format(
            "-2.0")
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["abc","."])
    def test_main_non_numeric_input(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()

        expected_output = "Введене значення не є числом."
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
