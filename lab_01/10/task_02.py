import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (cos(y) - cos(x))^2 - 5
Z2 = 4 * (sin((x - y) / 2*x))^2
Z3 = (5 + Z2) / Z1
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        z1 = (math.cos(y) - math.cos(x)) ** 2 - 5
        d = 2 * x
        if 2 * x != 0:
            z2 = 4 * (math.sin((x - y) / d)) ** 2
            if (math.cos(y) - math.cos(x)) ** 2 - 5 != 0:
                z3 = (5 + (4 * (math.sin((x - y) / 2 * x)) ** 2)) / ((math.cos(y) - math.cos(x)) ** 2 - 5)
                print(f"Z1 = {z1}")
                print(f"Z2 = {z2}")
                print(f"Z3 = {z3}")
                sys.exit(0)
            else:
                print("Некоректні дані. Значення Z1 не може бути рівним нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
